/**
 * Main file, load all files
 */
var ProjectModel = require('./src/models/project-model.js');
var ProjectView = require('./src/views/project-view');
var ProjectController = require('./src/controllers/project-controller');

var FolderModel = require('./src/models/folder-model');
var FolderView = require('./src/views/folder-view');
var FolderController = require('./src/controllers/folder-controller');

var SettingsModel = require('./src/models/settings-model');
var SettingsView = require('./src/views/settings-view');
var SettingsController = require('./src/controllers/settings-controller');

const app = {
    project: new ProjectController(new ProjectModel(), new ProjectView()),
    folder: new FolderController(new FolderModel(), new FolderView()),
    settings: new SettingsController(new SettingsModel(), new SettingsView())
}

require('./src/render.js');
