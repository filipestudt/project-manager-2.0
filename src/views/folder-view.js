class View {
    constructor() {
        this.root = document.getElementById('main');
    }

    render(data) {
        for (let folder of data) {
            var folderSpan = document.createElement('span');
            folderSpan.classList.add('folder');
            folderSpan.setAttribute('id', folder.id);
            folderSpan.innerHTML = folder.name || 'Undefined Folder';
            this.root.append(folderSpan);
        }
    }

    renderFolder(folder) {
        $('#name').val(folder.name);
        $('#new-project').addClass(folder.id);
    }

    bindView(handler) {
        $(document).on('click', '.folder', function() {
            handler(this.id);
        })
    }

    bindNew(handler) {
        $('#new-folder').click(function() {
            handler();
        })
    }

    bindEdit(handler) {
        $(document).on('focusout', '#name', function() {
            handler({
                name: this.value
            })
        })
    } 
}

module.exports = View;