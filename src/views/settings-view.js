class View {
    constructor() {
        this.defaultEditor = document.getElementById('default-editor');
        this.borderStyle = document.getElementById('border-style');
        this.windowSyle = document.getElementById('window-style');
        this.saveBtn = document.getElementById('save-settings');
    }

    render(data) {
        if (!data) return;

        if (data.defaultEditor) this.defaultEditor.value = data.defaultEditor;
        if (data.borderStyle) this.borderStyle.value = data.borderStyle;
        if (data.windowSyle) this.windowSyle.value = data.windowSyle;

        this.setBorderStyle(this.borderStyle.value);
        this.setWindowSyle(this.windowSyle.value);
    }

    bindSave(handler) {
        this.saveBtn.addEventListener('click', () => {
            handler({
                defaultEditor: this.defaultEditor.value,
                borderStyle: this.borderStyle.value,
                windowSyle: this.windowSyle.value
            })
            this.setBorderStyle(this.borderStyle.value);
            this.setWindowSyle(this.windowSyle.value);
        })
    }

    setBorderStyle(style) {
        if (style === 'rounded') {
            $('#left-bar').removeClass('square');
            $('#project-view').removeClass('square');
        }
        else if (style === 'square') {
            $('#left-bar').addClass('square');
            $('#project-view').addClass('square');
        }
    }

    setWindowSyle(style) {
        if (style === 'mac') {
            $('#menu').removeClass('windows');
            $('#menu').addClass('mac');
            /*
             * Change back the icons
             */
            $('#close').html('')
            $('#minimize').html('');
            $('#maximize').html('');
        }
        else {
            $('#menu').addClass('windows');
            $('#menu').removeClass('mac');
            /*
             * Change the icons
             */
            $('#close').html('<i class="fas fa-times"></i>')
            $('#minimize').html('<i class="fas fa-minus"></i>');
            $('#maximize').html('<i class="far fa-square"></i>');
        }
    }
}

module.exports = View;
