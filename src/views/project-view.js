class View {
    constructor() {
        this.root = document.getElementById('projects-list');
    }

    // Render the index
    renderIndex(data) {
        console.log(data)
        this.root.innerHTML = '';
        for (let prj of data) {
            var prjSpan = document.createElement('span');
            prjSpan.classList.add('project');
            prjSpan.setAttribute('id', prj.id);
            prjSpan.innerHTML = prj.title;
            this.root.append(prjSpan);
        }
    }

    // Render the project view
    renderProject(prj) {
        $('#icon').val(prj.icon)
        $('#title').val(prj.title)
        $('#description').val(prj.description)
        $('#dir').val(prj.dir)
        $('#command').val(prj.command)
        $('#project-id').val(prj.id)
        // seta como ativo
        $('.project').removeClass('active');
        $(`.project#${prj.id}`).addClass('active');
    }

    bindNew(handler) {
        $('#new').click(function() {
            handler();
        })
    }

    bindView(handler) {
        $(document).on('click', '.project', function() {
            handler(this.id);
        })
    }

    bindEdit(handler) {
        $('#icon').focusout(function() {
          var id = $('#project-id').val();
          handler({icon: this.value}, id)
        })
        $('#title').focusout(function() {
          var id = $('#project-id').val();
          handler({title: this.value}, id)
          // Refresh the new title of the project at the projects list
          $(`#${id}`).html(this.value);
        })
        $('#description').focusout(function() {
          var id = $('#project-id').val();
          handler({description: this.value}, id)
        })
        $('#dir').focusout(function() {
          var id = $('#project-id').val();
          handler({dir: this.value}, id)
        })
        $('#command').focusout(function() {
          var id = $('#project-id').val();
          handler({command: this.value}, id)
        })
        $('#folder').change(function() {
          var id = $('#project-id').val();
          handler({folderId: this.value}, id)
        })
    }

    bindDelete(handler) {
        $('#delete').click(function() {
            if (!confirm('Deseja realmente excluir o projeto?')) return;
            var id = $('#project-id').val();
            handler(id);
        })
    }

    bindExecute(handler) {
        $('#execute').click(function() {
            var id = $('#project-id').val();
            handler(id);

            // Visual changes
            if (! $('#execute').attr('class')) {
                $('#execute').html('<i class="fas fa-stop"></i> Parar');
                $('#execute').addClass('executing');
            } else {
                $('#execute').removeClass('executing');
                $('#execute').html('<i class="fas fa-play"></i> Executar');
            }
        })
    }

    bindOpenEditor(handler) {
        $('#open-vscode').click(function() {
            var id = $('#project-id').val();
            handler(id);
        })
    }

    bindOpenTerminal(handler) {
        $('#open-terminal').click(function() {
            var id = $('#project-id').val();
            handler(id);
        })
    }

    bindOpenExplorer(handler) {
        $('#open-explorer').click(function() {
            var id = $('#project-id').val();
            handler(id);
        })
    }

}

module.exports = View;
