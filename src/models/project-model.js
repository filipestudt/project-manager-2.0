const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)
const shortid = require('shortid')

db.defaults({ projects: [] })
  .write()

class Model {
    add(prj) {
        prj.id = shortid.generate()
        db.get('projects')
            .push(prj)
            .write()
    }

    get() {
        return db.get('projects')
        .value()
    }

    getById(id) {
        return db.get('projects')
        .find({id: id})
        .value()
    }

    getByFolder(folderId) {
        return db.get('projects')
        .filter({folderId: folderId})
        .value()
    }

    update(id, prj) {
        db.get('projects')
        .find({ id: id })
        .assign(prj)
        .write()
    }

    delete(id) {
        db.get('projects')
            .remove({id: id})
            .write()
    }

    save() {
        db.write()
    }
}

module.exports = Model;