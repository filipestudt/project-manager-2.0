const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)
const shortid = require('shortid')

db.defaults({ folders: [] })
  .write()

class Model {
    add(folder) {
        folder.id = shortid.generate()
        db.get('folders')
            .push(folder)
            .write()
    }

    get() {
        return db.get('folders')
        .value()
    }

    getById(id) {
        return db.get('folders')
        .find({id: id})
        .value()
    }

    update(id, folder) {
        db.get('folders')
        .find({ id: id })
        .assign(folder)
        .write()
    }

    delete(id) {
        db.get('folders')
            .remove({id: id})
            .write()
    }

    save() {
        db.write()
    }
}

module.exports = Model;