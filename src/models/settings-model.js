class Model {
    save(data) {
        localStorage.setItem('settings', JSON.stringify(data));
    }

    get() {
        return JSON.parse(localStorage.getItem('settings'));
    }

}

module.exports = Model;
