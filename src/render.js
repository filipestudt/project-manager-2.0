const remote = require('electron').remote;

// When document has loaded, initialise
document.onreadystatechange = () => {
    if (document.readyState == "complete") {
        handleWindowControls();
    }
};

function handleWindowControls() {

    let win = remote.getCurrentWindow();
    // Make minimise/maximise/restore/close buttons work when they are clicked
    document.getElementById('minimize').addEventListener("click", event => {
        win.minimize();
    });

    $(document).on('click', '#maximize', function() {
        this.id = 'restore';
        win.maximize();
        onMaximize();
    });

    $(document).on('click', '#restore', function() {
        this.id = 'maximize';
        win.unmaximize();
        onMinimize();
    })

    document.getElementById('close').addEventListener("click", event => {
        win.close();
    });


}

function onMaximize() {
    $('#left-bar').addClass('maximized')
    $('#main').addClass('maximized')
    $('#project-view').addClass('maximized')
}

function  onMinimize() {
    $('#left-bar').removeClass('maximized')
    $('#main').removeClass('maximized')
    $('#project-view').removeClass('maximized')
}
