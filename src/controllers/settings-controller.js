function Controller(model, view) {
    this.init = () => {
        var data = model.get();
        view.render(data);
    }

    this.handleSave = (data) => {
        model.save(data);
    }

    view.bindSave(this.handleSave);
    this.init();
}


module.exports = Controller;
