var GetURLParameter = require('../../lib/geturlparam');

function Controller(model, view) {
    this.init = async() => {
        this.id = GetURLParameter('id');

        if (!this.id) {
            var data = await model.get();
            view.render(data);
            return;
        }

        var folder = await model.getById(this.id);
        if (folder) {
            view.renderFolder(folder);
        }        
    }

    this.handleView = (id) => {
        window.location.href = 'folder.html?id=' + id;  
    }

    this.handleNew = () => {
        model.add({
            name: 'Nova Pasta'
        });
        location.reload();
    }

    this.handleEdit = (folder) => {
        model.update(this.id, folder);
        this.init();
    }

    view.bindNew(this.handleNew);
    view.bindView(this.handleView);
    view.bindEdit(this.handleEdit);
    this.init();
}

module.exports = Controller;