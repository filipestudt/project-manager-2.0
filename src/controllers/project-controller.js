var cmd = require('node-cmd');

function Controller(model, view) {

    this.init = async(prjParam) => {
        var data = await model.get();
        view.renderIndex(data);

        // also render the page of the project with the first project of the list
        // can also recives an project as parameter and render this project, this is used to render a new project when created
        view.renderProject(prjParam || data[0]);
    }

    this.handleView = async(id) => {
        var prj = await model.getById(id);
        view.renderProject(prj);
    }

    this.handleNew = async() => {
        model.add({
            title: 'Novo Projeto',
            icon: '🎈',
            description: 'Descrição do projeto'
        });

        // search for the created project
        var data = await model.get();
        var prj = data[data.length - 1];
        this.init(prj);
    }

    this.handleEdit = (data, id) => {
        model.update(id, data);
    }

    this.handleDelete = (id) => {
        model.delete(id);
        this.init();
    }

    /**
     *  This is a toggle function,
     *  if is already executing, it will stop
     */
    this.handleExecute = async(id) => {
        // If is executing, stop
        if (this.executing) {
            cmd.run('taskkill /F /T /PID ' + this.executionPid);
            this.executing = false;
            return;
        }

        var prj = await model.getById(id);

        // If not, execute the command and save the pid
        var process = cmd.run(`cd "${prj.dir}" && npm start`);
        this.executionPid = process.pid;
        this.executing = true;
    }

    this.handleOpenEditor = async(id) => {
        var prj = await model.getById(id);
        var defaultEditor = $('#settingsModal #default-editor').val();
        cmd.run(`${defaultEditor} "${prj.dir}"`);
    }

    this.handleOpenTerminal = async(id) => {
        var prj = await model.getById(id);
        cmd.run(`start cmd /k cd "${prj.dir}"`);
    }

    this.handleOpenExplorer = async(id) => {
        var prj = await model.getById(id);
        cmd.run(`explorer /e, "${prj.dir}"`);
    }

    view.bindView(this.handleView);
    view.bindNew(this.handleNew);
    view.bindEdit(this.handleEdit);
    view.bindDelete(this.handleDelete);
    view.bindExecute(this.handleExecute);
    view.bindOpenEditor(this.handleOpenEditor);
    view.bindOpenTerminal(this.handleOpenTerminal);
    view.bindOpenExplorer(this.handleOpenExplorer);
    this.init();

    // Some projects vars
    this.executing = false;
    this.executionPid = null;
}

module.exports = Controller;
