const { app, BrowserWindow } = require('electron')

function createWindow () {
  var win = new BrowserWindow({
    width: 800,
    height: 600,
    transparent: true, 
    frame: false,
    hasShadow: true,
    webPreferences: {
      nodeIntegration: true
    }
  })

  win.setMenuBarVisibility(false)
  
  win.loadFile('index.html')

  win.on('closed', function () {
    win = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})
