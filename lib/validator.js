function Validator() {

    var err;

    this.notNull = function(param, msg) {
        if (param === null || param === undefined || !param) {
            err = msg;
        }
    }

    this.hasLengthOf = function(param, len, msg) {
        if (param.length != len) {
            err = msg;
        }
    }

    this.hasLengthOfAtLeast = function(param, len, msg) {
        if (param.length < len) {
            err = msg;
        }
    }

    this.includes = function(param, arr, msg) {
        if (!arr.includes(param)) {
            err = msg;
        }
    }

    this.validate = function() {
        if (err) throw err;
    }
}

module.exports = Validator;